import os

POOLBOX_TOOLS_PATH = os.getenv("POOLBOX_TOOLS_PATH") or ""

EXTERNAL_TOOLS = [
    {"name": "pdktk", "command": "pdftk --version", "mandatory": True,},
    {"name": "wkhtmltopdf", "command": "wkhtmltopdf --version", "mandatory": False,},
    {"name": "pdftotext", "command": "pdftotext -v", "mandatory": False,},
]
