from subprocess import call

from pyramid.events import ApplicationCreated
from pyramid.events import subscriber
from poolbox.config import POOLBOX_TOOLS_PATH, EXTERNAL_TOOLS

import logging

log = logging.getLogger(__name__)


@subscriber(ApplicationCreated)
def check_external_tools(event):
    """."""
    log.info("Checking external tools availability")
    tools_path = POOLBOX_TOOLS_PATH or ""
    for tool in EXTERNAL_TOOLS:
        try:
            log.info("Checking %s" % tool["name"])
            call((tools_path + tool["command"]).split(" "))
        except OSError:
            if tool["mandatory"]:
                log.error(
                    "%s is not available on your environnement. Poolbox will not working"
                    % tool["name"]
                )
            else:
                log.warning(
                    "%s is not available on your environnement. Some functionnalites will not work properly"
                    % tool["name"]
                )
    log.info("Checking done")
