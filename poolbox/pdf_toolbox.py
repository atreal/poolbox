import io
import pdfkit
import re
import sys
import tempfile
from subprocess import call

from lxml import etree
from jinja2 import Environment, PackageLoader
from poolbox.config import POOLBOX_TOOLS_PATH as tools_path

import logging

log = logging.getLogger(__name__)


class PDFToolBox(object):
    """."""

    def __init__(self):
        """Check and set necessary tools."""
        pass

    def make_xfdf(self, fields):
        """Take the form dict and generate a .xfdf file with the values.

        :param fields: a dict of fields, like {FieldName: FieldsValue}
        :rtype: an absolute path to the xfdf file
        """
        root = etree.XML(
            # """<?xml version="1.0" encoding="UTF-8"?>
            """<?xml version="1.0"?>
            <xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">
            </xfdf>
        """
        )
        fields_node = etree.SubElement(root, "fields")

        for k, v in fields.items():
            field = etree.SubElement(fields_node, "field", name=k)
            value = etree.SubElement(field, "value")
            value.text = v

        xml = etree.tostring(
            root, encoding="UTF-8", xml_declaration=True, pretty_print=True
        )

        return self._write(xml, suffix=".xml")

    def fill_form(self, pdf_data, fields):
        """Take the pdf and the values and return a filled pdf.

        :param pdf_data: binary PDF data
        :param fields: a dict of fields, like {FieldName: FieldsValue}
        :rtype: filled binary PDF data
        """
        original_path = self._write(pdf_data)
        xfdf_path = self.make_xfdf(fields)
        filled_path = tempfile.mkstemp(suffix=".pdf")[1]

        call(
            [
                tools_path + "pdftk",
                original_path,
                "fill_form",
                xfdf_path,
                "output",
                filled_path,
            ]
        )

        with open(filled_path, "rb") as f:
            return f.read()

    def extract_form_fields(self, pdf_data, fields=None):
        """Take the pdf and extract fields value.

        :param pdf_data: binary PDF data
        :rtype: filled binary PDF data
        """

        pdf_path = self._write(pdf_data)
        fields_path = tempfile.mkstemp(suffix=".txt")[1]
        command = [
            tools_path + "pdftk",
            pdf_path,
            "dump_data_fields_utf8",
            "output",
            fields_path,
            ]
        call(command)
        fields = {}
        with open(fields_path, "r") as f:
            for item in re.split("---\n", str(f.read()))[1:]:
                field = {}
                for line in item.split("\n")[:-1]:
                    k, v = line.split(": ")
                    if k in ("FieldName", "FieldValue"):
                        field[k] = v
                try:
                    if "FieldValue" in field:
                        fields[field["FieldName"]] = field["FieldValue"]
                    else:
                        fields[field["FieldName"]] = ""
                except KeyError:
                    log.warning(
                        "Found a line without FieldName attribute. PDF does not seem valid"
                    )
        return fields

    def stamp_pdf(self, pdf_data, stamp_value):
        """Take the pdf, stamp it and return stamped pdf.

        :param pdf_data: binary PDF data
        :param stamp_value: a string to be used as the stamp
        :rtype: stamped binary PDF data
        """
        original_path = self._write(pdf_data)
        stamped_path = tempfile.mkstemp(suffix=".pdf")[1]
        stamp_path = self._write(
            self.render_pdf(
                "stamp.html.j2",
                {
                    "stamp_container_style": "text-align: right; font-family: arial,sans-serif",
                    "stamp_value": stamp_value,
                },
            )
        )

        call(
            [
                tools_path + "pdftk",
                original_path,
                "stamp",
                stamp_path,
                "output",
                stamped_path,
            ]
        )

        with open(stamped_path, "rb") as f:
            return f.read()

    def _write(self, data, suffix=".pdf"):
        """Write data in a temporary file and return the path.

        :param data: the data to be written
        :rtype: an absolute path to the file
        """
        path = tempfile.mkstemp(suffix=suffix)[1]
        with open(path, "wb") as f:
            f.write(data)
        return path

    def render_pdf(self, template, kwargs):
        """Allow you to render PDF based on HTML/J2 template."""
        j2_env = Environment(loader=PackageLoader("poolbox", "templates"))
        template = j2_env.get_template(template)
        rendered = template.render(kwargs)
        with io.StringIO(rendered) as html:
            return pdfkit.from_file(html, False)
